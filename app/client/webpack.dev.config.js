const {resolve} = require('path')
const webpack = require('webpack')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const AppManifestWebpackPlugin = require('app-manifest-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const config = {
    mode: 'development',

    devtool: 'source-map',

    entry: [
        '@babel/polyfill',
        './index.js'
    ],

    output: {
        filename: 'bundle.js',
        path: resolve(__dirname, 'dist'),
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.scss']
    },
    context: resolve(__dirname, 'src'),

    devServer: {
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
        },
        https: true,
        port: 3000,
        hot: true,
        publicPath: '/',
        historyApiFallback: {
            disableDotRule: true,
            rewrites: [
                {from: /^\/manifest.json$/, to: 'src/statics/manifest.json'},
            ]
        },
        proxy: {
            '/api/**': {
                target: 'http://localhost:3001',
                secure: false
            }
        }
    },

    module: {
        rules: [
            {
                test: /\.module\.s(a|c)ss$/,
                loader: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.s(a|c)ss$/,
                exclude: /\.module.(s(a|c)ss)$/,
                loader: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                loaders: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(jpg|jpeg|png|gif|ico|svg|pdf|eof)$/,
                use: 'file-loader?limit=15000&name=images/[name].[ext]&esModule=false'
            },
            {
                test: /\.eot(\?v=\d+.\d+.\d+)?$/,
                use: 'file-loader?&name=fonts/[name].[ext]'
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use:
                    'url-loader?limit=10000&mimetype=application/font-woff&name=fonts/[name].[ext]'
            },
            {
                test: /\.([ot]tf|ttf)(\?v=\d+.\d+.\d+)?$/,
                use:
                    'url-loader?limit=10000&mimetype=application/octet-stream&name=fonts/[name].[ext]'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use:
                    'url-loader?limit=10000&mimetype=image/svg+xml&name=images/[name].[ext]'
            }
        ]
    },

    plugins: [
        new HTMLWebpackPlugin({
            template: './index.html',
            favicon: './app/assets/favicon.ico',
            inject: 'body'
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new AppManifestWebpackPlugin({
            logo: '../app-icon.png',
            inject: true,
        })
    ]
}

module.exports = config
