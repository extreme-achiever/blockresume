/**
 * @flow
 */

import type {Action} from '../../app/types'
import type {DashboardState} from "./types";

const initialState = {
    resumeList: [],
    searchTerm: ''
}

function dashboard(state: DashboardState = initialState, action: Action): DashboardState {
    switch (action.type) {
        case 'FETCH_RESUME_LIST': {
            return {
                ...state,
            }
        }
        case 'SET_RESUME_LIST': {
            return {
                ...state,
                resumeList: action.resumeList
            }
        }
        case 'SET_SEARCH_TERM': {
            return {
                ...state,
                searchTerm: action.searchTerm
            }
        }
        default:
            return state
    }
}

export const appLoading = (state: { appLoading: Boolean } = { appLoading: false }, action: Action): { appLoading: Boolean } => {
    if (action.type === 'APP_LOADING') {
        state.appLoading = true
    }

    if (action.type === 'APP_LOADING_DONE') {
        state.appLoading = false
    }

    return {
        ...state
    }
}

export default dashboard
