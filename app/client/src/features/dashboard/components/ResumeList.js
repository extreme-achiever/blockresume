import React from 'react'
import ResumeThumbnail from "./ResumeThumbnail";
import CreateResumeThumbnail from "./CreateResumeThumbnail";
import style from '../stylesheets'

export default ({resumeList, onCreate, onEdit, onDelete, onDownload}) => {
    const listRendering = resumeList
        .map(
            resume => (<ResumeThumbnail key={resume.__id} {...{onEdit, onDelete, onDownload}} resume={resume}/>)
        )
    return (
        <div style={style.resumeListContainer}>
            <CreateResumeThumbnail {...{onCreate}} />
            {listRendering}
        </div>
    )
}
