import React from 'react'
import style from '../stylesheets'
import Icon from '@mdi/react'
import {mdiPlusCircle} from '@mdi/js'
import {Button, UncontrolledTooltip} from "reactstrap";

export default ({onCreate}) => {
    return (
        <Button id={`oncreate-button`} style={style.resumeThumbnailContainer} onClick={onCreate}>
            <Icon
                path={mdiPlusCircle}
                size={3}
                color={'white'}
            />
            <UncontrolledTooltip
                delay={0.5}
                target={`oncreate-button`}
                placement="top"
            >
                Create new resume
            </UncontrolledTooltip>
        </Button>
    )
}
