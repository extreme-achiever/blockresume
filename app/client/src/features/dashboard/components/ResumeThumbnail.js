import React from 'react'
import style from '../stylesheets'
import {Button, UncontrolledTooltip} from "reactstrap";
import Icon from '@mdi/react'
import { mdiDownload, mdiFileDocumentEdit, mdiDelete } from '@mdi/js'
import Moment from 'react-moment';

export default ({resume, onEdit, onDownload, onDelete}) => {
    const handleOnDownload = () => onDownload(resume)
    const handleOnEdit = () => onEdit(resume)
    const handleOnDelete = () => onDelete(resume)

    return (
        <div style={style.resumeContainer}>
            <div className="row col-12" style={style.resumeTextTitle}>{resume.resumeTitle}</div>
            <div className="row col-12" style={style.resumeTextCompany}>{resume.company}</div>
            <div className="row col-12" style={style.resumeTextUpdatedAt}>
                Updated <Moment fromNow>{resume.__updatedAt}</Moment>
            </div>
            <div style={style.resumeAction}>
                <Button size="sm" id={`ondowmload-${resume.__id}`} onClick={handleOnDownload}>
                    <Icon
                        path={mdiDownload}
                        size={1}
                        color={'white'}
                    />
                </Button>
                <UncontrolledTooltip
                    delay={0.5}
                    target={`ondowmload-${resume.__id}`}
                    placement="top"
                >
                    Download resume {resume.resumeTitle}
                </UncontrolledTooltip>
                <Button style={style.resumeActionButton} size="sm" id={`onedit-${resume.__id}`} onClick={handleOnEdit}>
                    <Icon
                        path={mdiFileDocumentEdit}
                        size={1}
                        color={'white'}
                    />
                </Button>
                <UncontrolledTooltip
                    delay={0.5}
                    target={`onedit-${resume.__id}`}
                    placement="top"
                >
                    Edit resume {resume.resumeTitle}
                </UncontrolledTooltip>
                <Button style={style.resumeActionButton} size="sm" id={`ondelete-${resume.__id}`}
                        onClick={handleOnDelete}>
                    <Icon
                        path={mdiDelete}
                        size={1}
                        color={'white'}
                    />
                </Button>
                <UncontrolledTooltip
                    delay={0.5}
                    target={`ondelete-${resume.__id}`}
                    placement="top"
                >
                    Delete resume {resume.resumeTitle}
                </UncontrolledTooltip>
            </div>
        </div>
    )
}
