import React from 'react'
import {Input} from 'reactstrap';
import styled from 'styled-components'
import Icon from '@mdi/react'
import {mdiCloudSearch, mdiCloseCircle} from '@mdi/js'

const StyledInputContainer = styled.div`
    height: 50px;
    background: #fff;
    align-items: center;
    border-radius: 10px
`

const ClearedStyleInput = styled(Input)`
    border: none!important;
    width: 80%!important;
    &:focus {
        border: none!important;
        box-shadow: none!important;
    }
`

const ClearableFlag = styled.div`
    cursor: pointer;
    position: absolute;
    right: 15px;
`

export default ({clearSearch, performSearch, searchTerm}) => {
    return (
        <div className={'col-6'}>
            <StyledInputContainer className={"row col-12"}>
                <Icon path={mdiCloudSearch} size={1}/>
                <ClearedStyleInput placeholder="Search resumes ..." value={searchTerm} onChange={performSearch}/>
                {searchTerm && (
                    <ClearableFlag onClick={clearSearch}>
                        <Icon path={mdiCloseCircle} size={1}/>
                    </ClearableFlag>
                )}
            </StyledInputContainer>
        </div>
    )
}
