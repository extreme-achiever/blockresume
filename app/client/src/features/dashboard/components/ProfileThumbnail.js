import React, {useState} from 'react'
import styled from 'styled-components'
import {withBlockstack} from "../../../common/blockstack";
import SampleAvatar from '../assets/common_avatar.png'
import {isEmpty} from 'ramda'
import {Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import {clearState} from "../../../app/actions";
import {connect} from 'react-redux'

const ThumbnailWrapper = styled.div`
   cursor: pointer;
`

const Image = styled.img`
    width: 60px;
    height: 60px;
    background: #fff;
    border-radius: 50%;
    margin-right: 15px;
`

const UsernameWrapper = styled.div`
    display: block;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    max-width: 10vw;
`

const ToggleWrapper = styled(DropdownToggle)`
    border: none!important;
    background: transparent!important;
    box-shadow: none!important;

    &:hover, &:active, &:focus {
        background: transparent!important;
        border: none!important;
        box-shadow: none!important;
    }
`

const CustomDropDownItem = styled(DropdownItem)`
    &:hover, &:focus, &:active{
        color: #ccc!important
    }
`

const ProfileThumbnail = (props) => {
    const {userSession} = props
    const userData = userSession.loadUserData()

    const getUserAvatar = (userData) => {
        if (!(!isEmpty(userData) && userData.profile.image && !isEmpty(userData.profile.image))) return SampleAvatar

        const images = userData.profile.image

        const avatar = images.filter(elm => elm.name === 'avatar')

        if (avatar) return avatar[0].contentUrl

        return SampleAvatar
    }


    const getUserDisplayName = (userData) => {
        if (isEmpty(userData)) return ''

        return userData.profile.name ? userData.profile.name : userData.username.split('.')[0]
    }

    const userAvatar = getUserAvatar(userData)
    const userDisplayName = getUserDisplayName(userData)

    const [dropdownOpen, setDropdownOpen] = useState(false);

    const toggle = () => setDropdownOpen(prevState => !prevState);

    const handleEditProfile = () => {
        window.open('https://browser.blockstack.org/profiles', "_blank")
    }

    const handleSignOut = () => {
        const {userSession} = props

        userSession.signUserOut()

        props.clearState()

        window.localStorage.clear()
        window.location.reload()
    }

    return (
        <div className="row col-12 align-items-center justify-content-end">
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                <ToggleWrapper>
                    <ThumbnailWrapper className="d-flex align-items-center">

                        <Image src={userAvatar}/>
                        <UsernameWrapper>
                            {userDisplayName}
                        </UsernameWrapper>

                    </ThumbnailWrapper>
                </ToggleWrapper>
                <DropdownMenu className="col-12">
                    <DropdownItem header>Profile</DropdownItem>
                    <CustomDropDownItem onClick={handleEditProfile}>Edit Profile</CustomDropDownItem>
                    <DropdownItem divider/>
                    <CustomDropDownItem onClick={handleSignOut}>Sign Out</CustomDropDownItem>
                </DropdownMenu>
            </Dropdown>
        </div>
    )
}

const mapActions = {
    clearState
}

export default connect(undefined, mapActions)(withBlockstack(ProfileThumbnail))
