import type {Action, AsyncAction} from "../../app/types";
import {ResumeData} from "../../common/model";

export const initFetchResumeList: Action = () => {
    return {
        type: 'FETCH_RESUME_LIST'
    }
}

export const setResumeList: Action = (resumeList) => {
    return {
        type: 'SET_RESUME_LIST',
        resumeList
    }
}

export const setSearchTerm: Action = (searchTerm) => {
    return {
        type: 'SET_SEARCH_TERM',
        searchTerm
    }
}

export function appLoading(): Action {
    return {
        type: 'APP_LOADING'
    }
}

export function appLoadingDone(): Action {
    return {
        type: 'APP_LOADING_DONE'
    }
}

export const callFetchResumeList: AsyncAction = () => {
    return async (dispatch) => {
        // inform that we start to init fetch resume list
        dispatch(initFetchResumeList())

        // now to fetch list
        ResumeData.fetchAll().then(resumeList => {
            dispatch(setResumeList(resumeList))
        })
    };
}
