import {colors} from "../../../common/theme";

export default {
    resumeListContainer: {
        width: 'calc(100%)',
        margin: '20px 10vw',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'start',
        flexWrap: 'wrap'
    },
    resumeContainer: {
        width: '15vw',
        maxWidth: '300px',
        maxHeight: '300px',
        height: '15vw',
        marginLeft: '10px',
        marginTop: '10px',
        background: '#fff',
        position: 'relative',
        borderRadius: '10px'
    },
    resumeThumbnailContainer: {
        width: '15vw',
        maxWidth: '300px',
        maxHeight: '300px',
        height: '15vw',
        marginLeft: '10px',
        marginTop: '10px',
        position: 'relative',
        borderRadius: '10px',
    },
    resumeTextTitle: {
        marginLeft: 0,
        marginTop: '15px',
        fontSize: '18px',
        fontWeight: 'bold',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        display: 'block',
        color: '#000'
    },
    resumeTextCompany: {
        marginLeft: 0,
        marginTop: '10px',
        fontSize: '13px',
        fontWeight: 'bold',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        display: 'block',
        color: '#000'
    },
    resumeTextUpdatedAt: {
        marginLeft: 0,
        marginTop: '20px',
        fontSize: '11px',
        fontWeight: 'bold',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        display: 'block',
        color: colors.background
    },
    resumeAction: {
        position: 'absolute',
        bottom: '0',
        background: 'rgba(59, 71, 96, 0.3)',
        padding: '10px',
        width: '100%',
        textAlign: 'center'
    },
    resumeActionButton: {
        marginLeft: '5px',
        zIndex: 99
    }
}
