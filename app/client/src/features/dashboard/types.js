/**
 * @flow
 */


type ResumeRecord = any

type ResumeList = Array<ResumeRecord>

type DashboardState = {
  resumeList: ResumeList,
  searchTerm: String
}

type DashboardAction =
    | { type: 'FETCH_RESUME_LIST' }
    | { type: 'SET_RESUME_LIST', resumeList: ResumeList }
    | { type: 'SET_SEARCH_TERM', searchTerm: String }

export type {  DashboardAction, DashboardState, ResumeList, ResumeRecord }
