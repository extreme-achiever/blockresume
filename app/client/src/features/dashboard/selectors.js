import * as R from 'ramda'
import type {State} from "../../app/types";


export const filteredResumes = (state: State) => {
    const {resumeList, searchTerm} = state.dashboard

    const matchedResume = (resume) => {
        const serializedResume = JSON.stringify(resume).toLowerCase()
        const _searchTerm = searchTerm?.toLowerCase() || ''
        return serializedResume.includes(_searchTerm)
    }

    return R.compose( // right-to-left operation
        R.sortWith([
            R.descend(R.prop('__updatedAt'))
        ]),
        R.filter(matchedResume)
    )(resumeList)
}
