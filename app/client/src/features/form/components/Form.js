/**
 * @flow
 */

import React, {Component} from 'react'
import {reduxForm} from 'redux-form'
import {connect} from 'react-redux'
import {Switch, Route, Redirect, type Location} from 'react-router-dom'
import styled from 'styled-components'
import {
    Templates,
    Profile,
    Education,
    Work,
    Skills,
    Projects,
    Awards,
    Overview
} from '.'
import Preview from '../../preview/components'
import {ScrollToTop} from '../../../common/components'
import {
    generateResume,
    resumeLoading,
    updateResume
} from '../../preview/actions'
import {setProgress} from '../../progress/actions'
import {colors} from '../../../common/theme'
import type {FormValues} from '../types'
import type {State} from '../../../app/types'
import type {Section} from '../../../common/types'
import {callFetchResumeList} from "../../dashboard/actions";
import $ from "jquery";

const StyledForm = styled.form`
  width: 40%;
  margin: 0;
  color: ${colors.white};
  padding: 25px 0 0 0;
  border-right: 1px solid ${colors.borders};
  overflow-y: auto;

  @media screen and (max-width: 850px) {
    width: 100%;
    border: none;
  }
`

type Props = {
    sections: Array<Section>,
    location: Location,
    handleSubmit: *,
    setProgress: (sections: Array<Section>, curr: Section) => void,
    generateResume: (payload: FormValues) => Promise<void>,
    match: any
}

class Form extends Component<Props> {
    form: ?HTMLFormElement

    componentWillMount() {
        if (this.props.progress === 0) {
            this.updateProgress()
        }
    }

    shouldComponentUpdate(prevProps) {
        return prevProps.location.pathname !== this.props.location.pathname
    }

    componentDidUpdate() {
        this.updateProgress()

        if (this.form) {
            this.form.scrollTop = 0
        }
    }

    onSubmit = async (values: FormValues) => {
        const {sections, resumeLoading, generateResume, currentResume, updateResume, callFetchResumeList} = this.props
        resumeLoading()
        await updateResume({...values, sections}, currentResume)
        callFetchResumeList()
        await generateResume({...values, sections})
    }

    updateProgress() {
        const {sections, location, setProgress, currentResume} = this.props

        if (
            !location.pathname.startsWith(`/generator/${currentResume?.__id ? currentResume?.__id : ''}`) ||
            location.pathname.includes('mobile')
        ) {
            return
        }


        const currSection: Section = (location.pathname.slice(11 + `${currentResume?.__id ? currentResume?.__id : ''}`.length + 1): any)
        setProgress(sections, currSection)
    }

    render() {
        const {handleSubmit, match: {params: {resumeId}}} = this.props
        return (
            <StyledForm
                id="resume-form"
                onSubmit={handleSubmit(this.onSubmit)}
                innerRef={form => (this.form = form)}
            >
                <ScrollToTop>
                    <Switch>
                        <Route
                            exact
                            path="/generator/:resumeId/"
                            render={() => <Redirect to={`/generator/${resumeId}/overview`}/>}
                        />
                        <Route exact path="/generator/:resumeId/overview" component={Overview}/>
                        <Route exact path="/generator/:resumeId/templates" component={Templates}/>
                        <Route exact path="/generator/:resumeId/profile" component={Profile}/>
                        <Route exact path="/generator/:resumeId/education" component={Education}/>
                        <Route exact path="/generator/:resumeId/work" component={Work}/>
                        <Route exact path="/generator/:resumeId/skills" component={Skills}/>
                        <Route exact path="/generator/:resumeId/projects" component={Projects}/>
                        <Route exact path="/generator/:resumeId/awards" component={Awards}/>
                        <Route exact path="/generator/:resumeId/mobile-preview" component={Preview}/>
                        <Route path="*" render={() => <h1 style={{margin: 0}}>404</h1>}/>
                    </Switch>
                </ScrollToTop>
            </StyledForm>
        )
    }
}

function mapState(state: State) {
    return {
        sections: state.progress.sections,
        progress: state.progress.progress
    }
}

const mapActions = {
    generateResume,
    setProgress,
    updateResume,
    resumeLoading,
    callFetchResumeList,
}

const ConnectedForm = connect(mapState, mapActions)(Form)

export default reduxForm({
    form: 'resume',
    destroyOnUnmount: false
})(ConnectedForm)
