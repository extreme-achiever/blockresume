/**
 * @flow
 */

import React from 'react'
import {Field} from 'redux-form'
import styled from 'styled-components'
import {darken} from 'polished'
import {colors} from '../../../../common/theme'
import {debounceSubmit} from "../../../../common/utils";

const Label = styled.label`
  display: block;
  margin-top: 30px;
  margin-bottom: 5px;
  color: ${colors.white};
  font-weight: 300;
`

const Input = styled(Field)`
  width: 100%;
  padding: 10px 0;
  appearance: none;
  outline: 0;
  font-size: 0.9em;
  font-family: inherit;
  border: none;
  border-bottom: 1px solid ${colors.borders};
  color: ${colors.white};
  transition: all 0.4s;
  background: transparent;
  outline: none;
  border-radius: 0;

  &:focus {
    color: ${colors.white};
    border-color: ${colors.primary};
  }

  &::placeholder {
    color: #7e899b;
    opacity: 0.4;
  }

  &:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 50px ${colors.background} inset;
    -webkit-text-fill-color: #fff;

    &:hover,
    &:focus,
    &:active {
      -webkit-box-shadow: 0 0 0 50px ${colors.background} inset;
      -webkit-text-fill-color: #fff;
    }

    &:focus {
      -webkit-text-fill-color: ${colors.white};
    }
  }
  
  &::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
      color: ${colors.primary};
      font-size: 10px;
      opacity: 1; /* Firefox */
  }

   &:-ms-input-placeholder { /* Internet Explorer 10-11 */
      color: ${colors.primary};
      font-size: 10px;
   }

    &::-ms-input-placeholder { /* Microsoft Edge */
      color: ${colors.primary};
      font-size: 10px;
    }

  @media screen and (max-width: 850px) {
    font-size: 16px;
    padding-left: 0;
    padding-right: 0;
    width: 100%;
  }
`

type Props = {
    label: string,
    name: string,
    placeholder: string,
    type?: string
}

function LabeledInput({label, name, placeholder, type = 'text'}: Props) {
    const debounceRender = () => {
        console.log('debounce')
        debounceSubmit()
    }
    return (
        <div>
            <Label>{label}</Label>
            <Input
                type={type}
                name={name}
                placeholder={`${placeholder} ...`}
                component="input"
                onChange={debounceRender}
            />
        </div>
    )
}

export {Label, Input}
export default LabeledInput
