/**
 * @flow
 */

import React from 'react'
import Section from './Section'
import LabeledInput from '../fragments/LabeledInput'

function Overview() {
    return (
        <Section heading="Your Resume Overview">
            <LabeledInput
                name="resumeTitle"
                label="Resume Title"
                placeholder="Name it easy to remember ..."
            />
            <LabeledInput
                name="company"
                label="Company"
                placeholder="Which company you create this resume for ..."
            />
        </Section>
    )
}

export default Overview
