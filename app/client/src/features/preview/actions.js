/**
 * @flow
 */

import FileSaver from 'file-saver'
import {isEqual} from 'lodash'
import type {PreviewAction as Action} from './types'
import type {FormValuesWithSectionOrder} from '../form/types'
import type {AsyncAction} from '../../app/types'
import {APP_URL_BASE} from "../../common/constant";
import {ResumeData} from "../../common/model";
import * as R from 'ramda'

function clearPreview(): Action {
    return {
        type: 'CLEAR_PREVIEW'
    }
}

function saveResumeData(data: FormValuesWithSectionOrder): Action {
    const {Blob, URL} = window
    const jsonString = JSON.stringify(data, null, 2)
    const blob = new Blob([jsonString], {type: 'application/json'})
    const url = URL.createObjectURL(blob)

    return {
        type: 'SAVE_RESUME_DATA',
        data,
        url
    }
}

function generateResumeRequest(): Action {
    return {
        type: 'GENERATE_RESUME_REQUEST'
    }
}
function resumeLoading(): Action {
    return {
        type: 'GENERATE_RESUME_LOADING'
    }
}

function generateResumeSuccess(resumeURL: string): Action {
    return {
        type: 'GENERATE_RESUME_SUCCESS',
        resumeURL
    }
}

function generateResumeFailure(): Action {
    return {
        type: 'GENERATE_RESUME_FAILURE'
    }
}

function generateResume(resumeData: FormValuesWithSectionOrder, forceDownload = false): AsyncAction {
    return async (dispatch, getState) => {
        const {resume, data} = getState().preview
        if (forceDownload === false && (resume.status === 'pending' || isEqual(data.json, resumeData))) {
            return
        }

        dispatch(generateResumeRequest())

        const {fetch, URL} = window
        const request = {
            method: 'POST',
            headers: {
                Accept: 'application/pdf',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(resumeData)
        }

        try {
            const response = await fetch(`${APP_URL_BASE}/api/generate/resume`, request)
            const blob = await response.blob()
            const url = URL.createObjectURL(blob)

            dispatch(generateResumeSuccess(url))

            if (forceDownload) window.open(url, '_blank')
        } catch (err) {
            dispatch(generateResumeFailure())
        }
    }
}

function updateResume(resumeData: FormValuesWithSectionOrder, currentResume: any): AsyncAction {
    return async (dispatch, getState) => {
        const {resume, data} = getState().preview
        if (resume.status === 'pending' || isEqual(data.json, resumeData)) {
            return
        }
        try {
            // update resume here
            // console.log('do what the fuck here')
            const _isNil = (obj) => obj.__id === null || obj.__id === undefined
            const predicateIsNil = val => R.eqBy(_isNil, {__id: val === true ? undefined : "dump"})

            const doCreateOrUpdate = R.cond([
                [predicateIsNil(true), (resume) => ResumeData.create(resume)],
                [predicateIsNil(false), (resume) => ResumeData.update(resume.__id, resume)],
                [R.T, async () => console.log('nothing to do')],
            ])

            const mergedObject = {...currentResume, ...resumeData}

            const resumeJson = await doCreateOrUpdate(mergedObject)

            dispatch(saveResumeData(resumeJson))
        } catch (err) {
            console.log(err)
        }
    }
}

function downloadSourceRequest(): Action {
    return {
        type: 'DOWNLOAD_SOURCE_REQUEST'
    }
}

function downloadSourceSuccess(): Action {
    return {
        type: 'DOWNLOAD_SOURCE_SUCCESS'
    }
}

function downloadSourceFailure(): Action {
    return {
        type: 'DOWNLOAD_SOURCE_FAILURE'
    }
}

function downloadSource(): AsyncAction {
    return async (dispatch, getState) => {
        const {fetch} = window
        const {resume, isDownloading, data} = getState().preview

        if (
            isDownloading ||
            resume.status === 'pending' ||
            data.json == null ||
            Object.keys(data.json).length === 0
        ) {
            return
        }

        dispatch(downloadSourceRequest())

        const jsonData = JSON.parse(JSON.stringify(data.json))

        delete jsonData['__id']
        delete jsonData['__createdAt']
        delete jsonData['__updatedAt']
        delete jsonData['__modelName']
        delete jsonData['__encrypt']
        delete jsonData['resumeTitle']
        delete jsonData['company']
        delete jsonData['selectedTemplate']
        delete jsonData['tags']
        delete jsonData['jsonResume']
        delete jsonData['resumeData']

        const req = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(jsonData),
            credentials: 'same-origin'
        }

        const res = await fetch(`${APP_URL_BASE}/api/generate/source`, req)
        const blob = await res.blob()

        if (!res.ok) {
            dispatch(downloadSourceFailure())
        } else {
            FileSaver.saveAs(blob, 'resume.zip')
            dispatch(downloadSourceSuccess())
        }
    }
}

export {
    clearPreview,
    generateResume,
    generateResumeRequest,
    generateResumeSuccess,
    generateResumeFailure,
    downloadSourceRequest,
    downloadSourceSuccess,
    downloadSourceFailure,
    downloadSource,
    updateResume,
    resumeLoading,
    saveResumeData
}
