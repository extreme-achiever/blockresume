/**
 * @flow
 */

type Section =
  | 'templates'
  | 'profile'
  | 'education'
  | 'work'
  | 'skills'
  | 'projects'
  | 'awards'
  | 'overview'


export type { Section }
