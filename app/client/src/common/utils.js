/**
 * @flow
 */

import $ from 'jquery'
import _ from 'lodash'

function titleCase(str: string): string {
    return str[0].toUpperCase() + str.slice(1)
}

export const debounceSubmit = _.debounce(() => {
    $('#btn-submit-form').click()
}, 300)

export {titleCase}
