import React from 'react'
import {AppConfig} from 'blockstack'
import ReactBlockstack, {useBlockstack} from 'react-blockstack'
import uuidV1 from 'uuid/v1'

export const APP_NAME = process.env.APP_NAME || "CommonStorage"

const appConfig = new AppConfig(['store_write', 'publish_data'])

const {userSession} = ReactBlockstack({appConfig})

export const withBlockstack = (Component) => (props) => {
    const {userData, signIn, signOut, person} = useBlockstack()

    const newProps = {...props, userSession, userData, signIn, signOut, person}

    return (<Component {...newProps}/>)
}


export const getOrCreateBaseObject = (fileName, encrypt = false) => {
    return new Promise((resolve, reject) => {
        userSession.getFile(`${APP_NAME}_${fileName}`, {decrypt: encrypt})
            .then((file) => {
                if (file !== null) return resolve(JSON.parse(file.toString()))

                userSession.putFile(`${APP_NAME}_${fileName}`, Buffer.from(JSON.stringify({})), {encrypt})
                    .then(() => resolve({}))
                    .catch(reject)
            })
            .catch(reject)
    })
}
export const putFile = (fileName, fileObject, encrypt = true) => {
    return new Promise((resolve, reject) => {
        userSession.putFile(`${APP_NAME}_${fileName}`, Buffer.from(JSON.stringify(fileObject)), {encrypt})
            .then(resolve)
            .catch(reject)
    })
}

const toJSON = (data) => {
    console.log(data)
    return JSON.parse(JSON.stringify(data))
}
export const getModel = (modelName, encrypt = true) => class Model {
    constructor(data = null) {
        this.__modelName = modelName
        this.__id = uuidV1().toString()
        this.__createdAt = new Date().getTime()
        this.__updatedAt = new Date().getTime()
        this.__encrypt = encrypt

        Object.assign(this, data)
    }

    static fetchAll() {
        return new Promise((resolve, reject) => {
            getOrCreateBaseObject(modelName, encrypt).then((fileData) => {
                const data = Object.keys(fileData).map(key => fileData[key])
                data.sort((elm2, elm1) => elm2.__createdAt - elm1.__createdAt)
                resolve(data)
            }).catch(reject)
        })
    }

    static getById(modelId) {
        return new Promise((resolve, reject) => {
            getOrCreateBaseObject(modelName, encrypt).then((data) => resolve(data[modelId])).catch(reject)
        })
    }

    static create(data) {
        return new Promise((resolve, reject) => {
            const object = new Model(data)
            getOrCreateBaseObject(modelName, encrypt).then((fileData) => {
                fileData[object.__id] = toJSON(object)

                putFile(modelName, fileData, encrypt)
                    .then(() => resolve(toJSON(object)))
                    .catch(reject)
            }).catch(reject)
        })
    }

    static update(modelId, data) {
        return new Promise((resolve, reject) => {
            getOrCreateBaseObject(modelName, encrypt).then((fileData) => {
                Object.assign(fileData[modelId], data)
                fileData[modelId].__updatedAt = new Date().getTime()
                putFile(modelName, fileData, encrypt)
                    .then(() => resolve(fileData[modelId]))
                    .catch(reject)
            }).catch(reject)
        })
    }

    static deleteById(modelId) {
        return new Promise((resolve, reject) => {
            getOrCreateBaseObject(modelName, encrypt).then((fileData) => {

                delete fileData[modelId]
                putFile(modelName, fileData, encrypt)
                    .then(() => resolve())
                    .catch(reject)
            }).catch(reject)
        })
    }
}
