import React from 'react'
import {connect} from 'react-redux'
import LoadingOverlay from 'react-loading-overlay'
import styled from 'styled-components'

const LoadingWrapper = styled.div`
    position: absolute;
    z-index: 999999;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0,0,0,0.4);
`

const LoadingOverlayDisplayWrapper = styled.div`
    position: relative;
    width: calc(100%);
    height: calc(100%);
`

const LoadingOverlayDisplay = styled(LoadingOverlay)`
    position: relative;
    width: calc(100%);
    height: calc(100%);
`

const Loading = ({appLoading}) => {
    if (appLoading === false) return null
    return (
        <LoadingWrapper>
            <LoadingOverlayDisplayWrapper>
                <LoadingOverlayDisplay
                    active={true} spinner text='Please wait ...'/>
            </LoadingOverlayDisplayWrapper>
        </LoadingWrapper>
    )
}

const mapState = (state) => ({
    appLoading: state.appLoading.appLoading
})

export default connect(mapState, undefined)(Loading)
