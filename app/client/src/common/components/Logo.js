/**
 * @flow
 */

import React from 'react'
import {Link} from 'react-router-dom'
import styled from 'styled-components'
import {colors} from '../theme'

const Text = styled.h1`
  font-family: PoppinsBold;
  font-size: ${props => (props.big ? 4 : 2.5)}em;
  margin: 0;
`

const StyledLink = styled(Link)`
  text-transform: lowercase;
  text-decoration: none!important;
  color: white!important;
  
  &:hover{
    color: white!important
  }
`
const StyledA = styled.a`
  text-transform: lowercase;
  text-decoration: none!important;
  color: white!important;
  cursor: pointer;
  
  &:hover{
    color: white!important
  }
`

const Accent = styled.em`
  font-style: normal;
  color: ${colors.primary};
`
const Image = styled.img`
    width: 70px;
    height: 70px
`;

const image = require('../../app/assets/app-icon.png')
type Props = {
    big?: boolean
}

function Logo({big, onClick}: Props) {
    return (
        <div onClick={onClick}>
            <Text style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}} big={big}>
                <Image src={image}/>
                {
                    onClick === undefined ?
                        (<StyledLink to="/">
                            block<Accent>resume</Accent>
                        </StyledLink>) :
                        (<StyledA>
                            block<Accent>resume</Accent>
                        </StyledA>)
                }
            </Text>
        </div>
    )
}

export default Logo
