/**
 * @flow
 */

import React from 'react'
import {Switch, Route} from 'react-router-dom'
import Loadable from 'react-loadable'
import {injectGlobal} from 'styled-components'
import {hot} from 'react-hot-loader'
import {ScrollToTop, Loader, AppLoading} from '../common/components'
import {colors} from '../common/theme'
import PoppinsBold from './assets/poppins/Poppins-Bold.ttf'
import PoppinsLight from './assets/poppins/Poppins-Light.ttf'
import PoppinsRegular from './assets/poppins/Poppins-Regular.ttf'
import {Blockstack} from 'react-blockstack/dist/context'
import '../common/blockstack'
import {withBlockstack} from "../common/blockstack";
import {RequiredLoginRoute} from "./router";

injectGlobal`
  * {
    box-sizing: border-box;
  }

  html, body, #app {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
    font-family: PoppinsRegular, -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
    font-size: 0.95em;
    background: ${colors.background};
    color: ${colors.foreground};
  }

  ::selection {
    background: ${colors.primary};
    color: white;
  }

  ::-moz-selection {
    background: ${colors.primary};
    color: white;
  }

  .grabbing {
    cursor: move; /* fallback */
    cursor: grabbing;
  }

  @font-face {
    font-family: PoppinsBold;
    src: url('${PoppinsBold}') format('opentype');
  }

  @font-face {
    font-family: PoppinsLight;
    src: url('${PoppinsLight}') format('opentype');
  }
  @font-face {
    font-family: PoppinsRegular;
    src: url('${PoppinsRegular}') format('opentype');
  }
`

const LoadableHome = Loadable({
    loader: () => import('./pages/Home'),
    loading: Loader
})

const LoadableGenerator = Loadable({
    loader: () => import('./pages/Generator'),
    loading: Loader
})

const LoadableDashboard = Loadable({
    loader: () => import('./pages/Dashboard'),
    loading: Loader
})

const LoadableAbout = Loadable({
    loader: () => import('./pages/About'),
    loading: Loader
})

const LoadableError404 = Loadable({
    loader: () => import('./pages/Error404'),
    loading: Loader
})

function App(props) {
    window.Props = props
    return (
        <Blockstack>
            <AppLoading />
            <ScrollToTop>
                <Switch>
                    <Route exact path="/" component={LoadableHome}/>
                    <RequiredLoginRoute path="/my-resumes" renderComponent={LoadableDashboard}/>
                    <RequiredLoginRoute path="/generator/:resumeId" renderComponent={LoadableGenerator}/>
                    <Route path="/about" component={LoadableAbout}/>
                    <Route path="*" component={LoadableError404}/>
                </Switch>
            </ScrollToTop>
        </Blockstack>
    )
}

export default hot(module)(withBlockstack(App))
