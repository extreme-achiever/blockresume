/**
 * @flow
 */

import React, {useEffect, useState} from 'react'
import {connect} from 'react-redux'
import Loadable from 'react-loadable'
import styled from 'styled-components'
import Form from '../../features/form/components'
import {SideNav, Progress} from '../../features/progress/components'
import {Logo, Loader} from '../../common/components'
import {colors, sizes} from '../../common/theme'
import type {Location} from 'react-router-dom'
import {withBlockstack} from "../../common/blockstack";
import Icon from "@mdi/react";
import {mdiArrowLeft} from "@mdi/js";
import {UncontrolledTooltip} from "reactstrap";
import ProfileThumbnail from "../../features/dashboard/components/ProfileThumbnail";
import 'bootstrap/dist/css/bootstrap.css';
import * as R from 'ramda'
import {uploadJSONSuccess} from "../../features/form/actions";
import {generateResume} from "../../features/preview/actions";
import {debounceSubmit} from "../../common/utils";
import LoadingOverlay from 'react-loading-overlay';

const Layout = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  
  & button {
    box-shadow: none!important;
    outline: none!important;
    
    &:active, &:focus, &:hover {
     background: inherit!important;
     color: #fff
    }
    
    &:disabled {
     border-color: ${colors.foreground};;
     color: ${colors.foreground};
     
     &:disabled:hover {
      border-color: ${colors.foreground};;
      color: ${colors.foreground};
     }
    }
  }
  
  & a:hover{
    color: inherit!important;
    text-decoration: none!important;
  }
  
  & a.active::before {
    width: 100%;
  }
`

const Content = styled.main`
  display: flex;
  margin-top: ${sizes.header};
  margin-left: ${sizes.sideNav};
  width: calc(100% - ${sizes.sideNav});
  height: calc(100% - ${sizes.header} - ${sizes.footer} - 2px);

  @media screen and (max-width: 850px) {
    width: 100%;
    margin-left: 0;
    flex-direction: column;
  }
`

const Header = styled.header`
  position: fixed;
  z-index: 1;
  width: 100vw;
  height: ${sizes.header};
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${colors.background};
  border-bottom: 1px solid ${colors.borders};
`

const Footer = styled.footer`
  width: 100%;
  height: ${sizes.footer};
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  background: ${colors.background};
  border-top: 1px solid ${colors.borders};
`

const IconHomeWrapper = styled.div`
  cursor: pointer;
`

const LoadablePreview = Loadable({
    loader: () => import('../../features/preview/components'),
    loading: Loader
})

type Props = {
    location: Location
}

function Generator({location, resumeList, uploadJSONSuccess, generateResume, ...rest}: Props) {
    const [currentResume, setCurrentResume] = useState(null)
    const handleRedirect = (path) => ($event) => {
        $event.stopPropagation()
        $event.preventDefault()

        window.location = `https://${window.location.host}/${path}`
    }

    const handleRedirectDashboard = ($event) => {
        $event.preventDefault()
        $event.stopPropagation()

        return handleRedirect('my-resumes')($event)
    }

    const handleRedirectHomePage = ($event) => {
        $event.preventDefault()
        $event.stopPropagation()

        return handleRedirect('')($event)
    }

    useEffect(() => {
        const {resumeId} = rest.match.params;
        const resumeObject = R.find(R.propEq('__id', resumeId))(resumeList)

        if (resumeObject === undefined) {
            return rest.history.push('/404')
        }

        setCurrentResume(resumeObject)

        uploadJSONSuccess(resumeObject)

        setTimeout(() => {
            debounceSubmit()
        }, 300)
    }, []);

    return (
        <Layout>
            <Header>
                <div className="row col-5 justify-content-start">
                    <Logo onClick={handleRedirectHomePage}/>
                </div>
                <IconHomeWrapper id="IconHome" onClick={handleRedirectDashboard}
                                 className="row col-2 justify-content-center ml-2 mr-2">
                    <Icon path={mdiArrowLeft} color={colors.primary} size={2}/>
                    <UncontrolledTooltip
                        delay={0.5}
                        target="IconHome"
                        placement="right"
                    >
                        Go To My Resumes
                    </UncontrolledTooltip>
                </IconHomeWrapper>
                <div className="row col-5 justify-content-end">
                    <ProfileThumbnail/>
                </div>
            </Header>
            <SideNav currentResume={currentResume} {...rest} />
            <Content>
                <Form {...rest} currentResume={currentResume} location={location}/>
                <LoadablePreview hideOnMobile/>
            </Content>
            <Footer>
                <Progress currentResume={currentResume}/>
            </Footer>
        </Layout>
    )
}


const mapState = (state) => ({
    resumeList: state.dashboard.resumeList
})

const mapActions = {
    uploadJSONSuccess,
    generateResume
}

export default connect(mapState, mapActions)(withBlockstack(Generator))
