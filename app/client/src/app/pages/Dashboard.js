/**
 * @flow
 */

import React, {useEffect} from 'react'
import styled from 'styled-components'
import {Logo} from '../../common/components'
import {colors, sizes} from '../../common/theme'
import type {Location} from 'react-router-dom'
import {withBlockstack} from "../../common/blockstack";
import {appLoading, appLoadingDone, callFetchResumeList, setSearchTerm} from "../../features/dashboard/actions";
import ResumeList from "../../features/dashboard/components/ResumeList";
import SearchBar from "../../features/dashboard/components/SearchBar";
import {ResumeList as ResumeListType} from "../../features/dashboard/types";
import type {AsyncAction, Action} from "../types";
import {connect} from 'react-redux'
import {ResumeData} from "../../common/model";
import 'bootstrap/dist/css/bootstrap.css';
import {filteredResumes} from "../../features/dashboard/selectors";
import ProfileThumbnail from "../../features/dashboard/components/ProfileThumbnail";
import Icon from '@mdi/react'
import {mdiArrowLeft} from '@mdi/js'
import {UncontrolledTooltip} from 'reactstrap'
import {initialState} from "../../features/form/reducer";
import {clearPreview, generateResume, saveResumeData} from "../../features/preview/actions";

const Layout = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`

const Content = styled.main`
  display: flex;
  // margin-top: ${sizes.header};
  padding-bottom: 50px;
  width: calc(100%);
  height: auto;
  
  @media screen and (max-width: 850px) {
    width: 100%;
    margin-left: 0;
    flex-direction: column;
  }
`

const Header = styled.header`
  position: fixed;
  z-index: 1;
  width: 100vw;
  height: ${sizes.header};
  display: flex;
  justify-content: start;
  padding-left: 40px;
  align-items: center;
  background: ${colors.background};
  border-bottom: 1px solid ${colors.borders};
`


const DashboardTitle = styled.div`
  margin-top: calc(${sizes.header} + 5vh);
  // background: ${colors.background};
  // width: 100vw;
  // z-index: 1;
  // position: fixed;
  // height: ${sizes.header};
  text-align: center;
`

const SearchBarContainer = styled.div`
  margin-top: 20px;
  justify-content: center;
  display: flex;
  text-align: center;
`

const IconHomeWrapper = styled.div`
  cursor: pointer;
`

type Props = {
    location: Location,
    resumeList: ResumeListType,
    callFetchResumeList: AsyncAction,
    setSearchTerm: Action
}

function Generator({appLoadingDone, appLoading, callFetchResumeList, generateResume, clearPreview, previewData, saveResumeData, resumeList, setSearchTerm, searchTerm, ...rest}: Props) {
    const constructResume = () => ({
        ...initialState.values,
        resumeTitle: 'Untitled',
        company: 'Untitled Company',
        tags: [],
    })

    const performSearch = ($event) => {
        const {value} = $event.target
        setSearchTerm(value)
    }

    const clearSearch = () => {
        setSearchTerm('')
    }

    const onCreate = () => {
        const newResume = constructResume()

        appLoading()

        ResumeData.create(newResume).then(async (createdResume) => {
            await callFetchResumeList()

            appLoadingDone()

            setTimeout(() => {
                onEdit(createdResume)
            }, 1000)
        })
    }

    const onEdit = (resume) => {
        console.log('edit', resume.__id)
        return rest.history.push(`/generator/${resume.__id}`)
    }

    const onDelete = (resume) => {
        const confirm = window.confirm(`Do you want to delete ${resume.resumeTitle} ?`)

        if (confirm === false) return false

        appLoading()
        ResumeData.deleteById(resume.__id).then(() => {
            callFetchResumeList().then(() => appLoadingDone())
        })
    }

    const onDownload = async (resume) => {
        console.log('download', resume.__id)
        appLoading()
        saveResumeData(resume)
        await generateResume(resume, true)
        appLoadingDone()
    }

    const handleRedirect = (path) => ($event) => {
        $event.stopPropagation()
        $event.preventDefault()

        window.location = `https://${window.location.host}/${path}`
    }

    const handleRedirectHomePage = ($event) => {
        $event.preventDefault()
        $event.stopPropagation()

        return handleRedirect('')($event)
    }

    const resumeListProps = {
        resumeList,
        onCreate,
        onDelete,
        onEdit,
        onDownload
    }

    useEffect(() => {
        // component did mount
        // call fetch resume list
        appLoading()
        callFetchResumeList().then(() => {
            appLoadingDone()
        })
    }, [])


    return (
        <Layout>
            <Header>
                <div className="row col-5 justify-content-start">
                    <Logo onClick={handleRedirectHomePage}/>
                </div>
                <IconHomeWrapper id="IconHome" onClick={handleRedirectHomePage}
                                 className="row col-2 justify-content-center ml-2 mr-2">
                    <Icon path={mdiArrowLeft} color={colors.primary} size={2}/>
                    <UncontrolledTooltip
                        delay={0.5}
                        target="IconHome"
                        placement="right"
                    >
                        Go Home
                    </UncontrolledTooltip>
                </IconHomeWrapper>
                <div className="row col-5 justify-content-end">
                    <ProfileThumbnail/>
                </div>
            </Header>
            <DashboardTitle>
                <h3>My Resumes</h3>
            </DashboardTitle>
            <SearchBarContainer>
                <SearchBar clearSearch={clearSearch} performSearch={performSearch} searchTerm={searchTerm}/>
            </SearchBarContainer>
            <Content>
                <ResumeList {...resumeListProps} />
            </Content>
        </Layout>
    )
}

const mapActions = {
    callFetchResumeList,
    setSearchTerm,
    saveResumeData,
    clearPreview,
    generateResume,
    appLoading,
    appLoadingDone
}

const mapState = (state) => ({
    resumeList: filteredResumes(state),
    // resumeList: state.dashboard.resumeList || [],
    searchTerm: state.dashboard.searchTerm,
    previewData: state.preview.resume
})

export default connect(mapState, mapActions)(withBlockstack(Generator))
