import React from 'react'
import {Redirect, Route} from "react-router-dom";
import {withBlockstack} from "../common/blockstack";

const _requireLoginRoute = ({renderComponent: ChildrenComponent, ...routerProps}) => {
    const {userSession} = routerProps

    // console.log('what the fuck')

    return (
        <Route {...routerProps} render={(props) =>
            userSession.isUserSignedIn() ? (<ChildrenComponent {...props} />) : (<Redirect
                to={{
                    pathname: "/",
                    state: {from: props.location}
                }}
            />)
        }/>
    );
}


export const RequiredLoginRoute = withBlockstack(_requireLoginRoute)
